import * as express from 'express';

import { Bridge, BridgeModule, get, httpController, post, put, enums, fromParam, fromBody, httpDelete } from '../';

import { enums as e } from '@atlas/common';


/**
 * Product list for testing
 */
const products: Product[] = [
    { id: 1, name: 'Product 1', price: 10 },
    { id: 2, name: 'Product 2', price: 18 },
    { id: 3, name: 'Product 3', price: 24 }
];

/**
 * Type definition for product
 */
class Product {
    id!: number;
    name!: string;
    price!: number;
}

/**
 * Simple Rest Product's Controller
 */
@httpController('/products')
export class ProductsController {

    @get('/')
    private index(req: express.Request, res: express.Response, next: express.NextFunction): Promise<Product[]> {
        return Promise.resolve(products);
    }

    @get('/:id')
    private getById(id: string): Promise<Product | undefined> {
        return Promise.resolve(products.find(p => p.id === +id));
    }

    @post('/')
    private add(@fromBody product: Product): Promise<Product> {
        products.push(product);
        return Promise.resolve(product);
    }

    @put('/')
    private put(@fromBody name: string, @fromBody price: number): Promise<Product | null> {
        var productIndex = products.findIndex(p => p.name === name);

        if (productIndex === -1) return Promise.resolve(null);

        products[productIndex] = { id: 5, name: name, price: price };

        return Promise.resolve(products[productIndex]);
    }

    @httpDelete('/:id')
    private delete(id: string): Promise<boolean> {
        var productIndex = products.findIndex(p => p.id === +id);

        if (productIndex === -1) return Promise.resolve(false);

        products.splice(productIndex, 1);

        return Promise.resolve(true);
    }
}

/**
 * Bridge Module definition 
 */
@BridgeModule({
    httpControllers: [ ProductsController ]
})
class AppModule {}

// Create the app
Bridge.setLoggerLevel(e.LoggerLevelEnum.debug);
const app = Bridge.create(AppModule);

if (app)
    app.start();


/*
   How to test it from the terminal using curl

   - Get all product
   
   curl http://localhost:9091/products

   - Get specific product

   curl -X GET \
    http://localhost:9091/products/2 \
    -H 'Cache-Control: no-cache'

   - Add new product

   curl \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{ "product": "{ products { name price } }" }' \
    http://127.0.0.1:9091/graphql
  
 */