import { Bridge } from '../lib/bridge';
import { BridgeModule, GqlField, GqlQuery, GqlType, GqlMutation, GqlInput } from '../lib/decorators';
import { enums } from '../lib/enums';
import { IQuery } from '../lib/queries/query';
import { IMutation } from '../lib/mutations/mutation';

const products: Product[] = [
    { name: 'Product 1', price: 10 },
    { name: 'Product 2', price: 18 },
    { name: 'Product 3', price: 24 }
];

/**
 * Type definition for product
 */
@GqlType()
class Product {

    // Default data type for every field is String
    // this could also be used: @GqlField({ type: GqlTypeEnum.String })
    @GqlField()
    name!: string;

    @GqlField({ type: enums.GqlTypeEnum.Float })
    price!: number;
}

@GqlInput()
class ProductInput extends Product { }

/**
 * Simple Query to return List of products
 */
@GqlQuery({
    name: 'products',
    output: { type: Product, isArray: true }
})
class GetProductsQuery implements IQuery<Product[]> {
    
    run(data: any): Promise<Product[]> {
        return Promise.resolve(products);
    }
}

@GqlMutation({
    name: 'addProduct',
    parameters: [
        { name: 'input', type: ProductInput }
    ],
    output: { type: Product }
})
class AddProductMutation implements IMutation<Product> {
    
    run(data: { input: ProductInput }): Promise<Product> {
        products.push(data.input);
        return Promise.resolve(data.input);
    }
}

/**
 * Bridge Module definition 
 */
@BridgeModule({
    queries: [ GetProductsQuery ],
    mutations: [ AddProductMutation ]
})
class AppModule {}

// Create the app
const app = Bridge.create(AppModule);

if (app)
    app.start();


// How to test it from console using "curl"
/*
  
 // Get Products 

 curl \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{ "query": "{ products { name price } }" }' \
  http://127.0.0.1:9091/graphql

  // Add new Product

  curl \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{ "query": "mutation { addProduct(input: { name: \"New Product\", price: 25 }) { name price } }" }' \
  http://127.0.0.1:9091/graphql
  
 */