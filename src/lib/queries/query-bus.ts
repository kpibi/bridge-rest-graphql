import { Bridge } from '../bridge';
import { Request } from 'express';
import { inject, injectable } from 'inversify';

import { IBridgeRequest } from '../bridge.request';
import { Enforcer, IEnforcer } from '../enforcer';
import { interfaces } from '../interfaces';
import { IQuery } from './query';


export interface IQueryBus {
    run<T>(req: Request, query: IQuery<T>, data: any, activity?: new () => interfaces.IActivity): Promise<any>;
}

@injectable()
export class QueryBus implements IQueryBus {

    authorizedValue: any;
    errorBool: any;
    errorStr: any;
    logParams: any;

    public get enforcer():  IEnforcer {
        return this._enforcer;
    }

    constructor(@inject(Enforcer.name) private _enforcer: IEnforcer) { }

    run<T>(request: IBridgeRequest, query: IQuery<T>, data: any, activity?: new () => interfaces.IActivity): Promise<any> {
        const that = this;

        if (!activity) {
            return (query as any).run(data)
                .then((data: any) => data)
                .catch((err: any) => {
                    Bridge.logger.error(query.constructor.name, err);
                    return err;
                });
        }

        // get activity instance
        const act: interfaces.IActivity = request.Container.instance.get(activity.name);

        // chack activity authorization
        return this.enforcer.authorizationTo(act)
            .then((authorized) => {
                if (!authorized) {
                    return Promise.reject(authorized);
                }

                return Promise.resolve(true);
            })
            .then((authorized: boolean) => {
                that.authorizedValue = authorized;
                if (authorized) {
                    console.log('trying to run query: ' + query.constructor.name);
                    return (query as any).run(data)
                        .then((data: any) => data)
                        .catch((err: any) => {
                            Bridge.logger.error(query.constructor.name, err);
                        });
                }
            })
            .catch((err) => {
                that.errorStr = err;
                return Promise.resolve(err);
            });

    }

    // private _logQuery(request: IBridgeRequest, query) {
    //     if ((query.log === true) && request && request) {
    //         const user = request.user;
    //         const accessBy = user ? user.profile.firstName + ' ' + user.profile.lastName : '';

    //         const logParams = {
    //             timestamp: Date.now(),
    //             accessBy: accessBy,
    //             ipAddress: request.connection ? request.connection.remoteAddress : '',
    //             event: query.constructor.name,
    //             clientDetails: request.get('User-Agent'),
    //             eventType: 'query',
    //             payload: JSON.stringify(request.body),
    //             results: {
    //                 authorized: this.authorizedValue,
    //                 status: true,
    //                 details: this.errorStr || ('Success executing ' + query.constructor.name)
    //             }
    //         };
    //         const accessLogs = request.Container.instance.get<AccessLogs>(AccessLogs.name);

    //         accessLogs.model.create(this.logParams);
    //     }
    // }
}