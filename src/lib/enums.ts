export namespace enums {

    export enum HttpVerbEnum {
        Get = 'get',
        Post = 'post',
        Put = 'put',
        Patch = 'patch',
        Delete = 'delete'
    }

    export enum GraphqlMetaType {
        Input,
        Type,
        Query,
        Mutation
    }

    export enum GqlTypeEnum {
        String = 'String',
        Int = 'Int',
        Float = 'Float',
        Boolean = 'Boolean'
    };

    export enum FromTypeEnum {
        query = 'query',
        param = 'param',
        body = 'body',
        header = 'header'
    }
    
}

