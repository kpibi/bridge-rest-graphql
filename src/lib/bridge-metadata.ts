import { IQuery } from './queries/query';
import { METADATA_KEY } from './constants';
import { interfaces } from "./interfaces";
import { values } from 'lodash';
import { decorate, injectable } from 'inversify';

import { interfaces as i } from '@atlas/common';


const _graphql: interfaces.IBrigeData = {
    types: {},
    inputs: {},
    queries: {},
    mutations: {},
    modules: []
}

const _http = {
    controllers: {} as interfaces.IHttpControllers
}

export function _addType(type: i.Newable < any > , graphqlText: string) {
    if (!type) return;

    Reflect.defineMetadata(METADATA_KEY.type, graphqlText, type);
    _graphql.types[type.name] = type;
}

export function _getTypes(): i.Newable < any > [] {
    return values(_graphql.types);
}

export function _getTypeMetadata(type: i.Newable < any > ): interfaces.IInputMetadata {
    const gqlText = Reflect.getMetadata(METADATA_KEY.type, type);
    const resolvers = Reflect.getMetadata(METADATA_KEY.resolver, type);

    return {
        name: type.name,
        text: gqlText,
        resolver: resolvers
    };
}

export function _addInput(input: i.Newable < any > , graphqlText: string) {
    if (!input) return;

    Reflect.defineMetadata(METADATA_KEY.input, graphqlText, input);
    _graphql.inputs[input.name] = input;
}

export function _getInputs(): i.Newable < any > [] {
    return values(_graphql.inputs);
}

export function _getInputMetadata(input: i.Newable < any > ): string {
    const graphqlText = Reflect.getMetadata(METADATA_KEY.input, input);
    return graphqlText;
}

export function _addQuery(query: i.Newable < any > , metadata: interfaces.IActionMetadata) {
    if (!query) return;

    const resolver = function _executeResolver(root: any, args: any, ctx: interfaces.IGraphqlContext) {
        // get an intance using the dependency injection container
        console.log(`Resolving query: ${query.name}`);
        const i = ctx.requestContainer.instance.get(query.name) as any;
        const metadata = _getQueryMetadata(query);

        return ctx.queryBus.run(ctx.req, i, args, metadata.activity);
    };

    Reflect.defineMetadata(METADATA_KEY.query, Object.assign(metadata, {
        resolver
    }), query);
    decorate(injectable(), query);

    _graphql.queries[query.name] = query;
}

export function _getQueries(): i.Newable < IQuery < any >> [] {
    let queries: i.Newable < IQuery < any >> [] = [];

    _graphql.modules.forEach(mod => {
        const modMetadata = _getModuleMetadata(mod);

        if (modMetadata.queries) {
            queries = queries.concat(modMetadata.queries.filter(q => q !== undefined));
        }
    });

    return queries;
}

export function _getQueryMetadata(query: i.Newable < any > ): interfaces.IActionMetadata {
    return Reflect.getMetadata(METADATA_KEY.query, query);
}

export function _addMutation(mutation: any, metadata: interfaces.IActionMetadata) {
    if (!mutation) return;

    const resolver = function _executeResolver(root: any, args: any, ctx: interfaces.IGraphqlContext) {
        // get an intance using the dependency injection container
        console.log(`Resolving mutation: ${mutation.name}`);
        const i = ctx.requestContainer.instance.get(mutation.name) as any;
        const metadata = _getMutationMetadata(mutation);

        return ctx.mutationBus.run(ctx.req, i, args, metadata.activity);
    };

    Reflect.defineMetadata(METADATA_KEY.mutation, Object.assign(metadata, {
        resolver
    }), mutation);
    decorate(injectable(), mutation);

    _graphql.mutations[mutation.name] = mutation;
}

export function _getMutations(): i.Newable < any > [] {
    let mutations: i.Newable < any > [] = [];

    _graphql.modules.forEach(mod => {
        const modMetadata = _getModuleMetadata(mod);

        if (modMetadata.mutations) {
            mutations = mutations.concat(modMetadata.mutations.filter(m => m !== undefined));
        }
    });

    return mutations;
}

export function _getMutationMetadata(mutation: i.Newable < any > ): interfaces.IActionMetadata {
    return Reflect.getMetadata(METADATA_KEY.mutation, mutation);
}

export function _addModule(mod: any, options: interfaces.IModuleDecorator): any {
    if (!mod) return;

    Reflect.defineMetadata(METADATA_KEY.module, options, mod);
    _graphql.modules.push(mod);
}

export function _getModules(): Function[] {
    return _graphql.modules;
}

export function _getModuleMetadata(mod: i.Newable < interfaces.IAppModule > ): interfaces.IModuleDecorator {
    return Reflect.getMetadata(METADATA_KEY.module, mod);
}

export function _addHttpController(controller: any, metadata: interfaces.IHttpControllerDecorator) {
    if (!controller) return;

    Reflect.defineMetadata(METADATA_KEY.controller, metadata, controller);
    decorate(injectable(), controller);

    _http.controllers[controller.name] = controller;
}

export function _getControllerMetadata(controller: i.Newable < any > ): interfaces.IHttpControllerDecorator {
    return Reflect.getMetadata(METADATA_KEY.controller, controller);
}

let _schema: string;

export function _saveSchema(schema: string) {
    _schema = schema;
}