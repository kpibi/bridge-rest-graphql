import { Request } from 'express';
import { Container, interfaces as inversifyInterfaces } from 'inversify';

import { interfaces } from './interfaces';
import { interfaces as i } from '@atlas/common';


export class BridgeContainer implements interfaces.IBridgeContainer {
    private _containerModules: interfaces.IBridgeContainer[];
    
    constructor(private _container: Container) {
        this._containerModules = [];
    }

    register<T extends i.Newable<any>>(type: T): void {
        this._container.bind<T>(type.name).to(type);
    }

    registerSingleton<T extends i.Newable<any>>(type: T): void {
        this._container.bind<T>(type.name).to(type).inSingletonScope();
    }

    registerPerWebRequest<T extends i.Newable<any>>(type: T): void {
        if (!type) {
            throw new Error('A type is required for the registration');
        }

        this._container.bind<T>(type.name).to(type).inRequestScope();
    }

    registerConstant<T extends interfaces.GenericObject>(identifier: string, value: T): void {
        this._container.bind<T>(identifier).toConstantValue(value);
    }

    deregister<T extends i.Newable<any>>(identifier: string): void {
        this._container.unbind(identifier);
    }

    get<T>(identifier: string): T {
        return this._container.get(identifier);
    }

    getBridgeContainerForWebRequest(req: Request): interfaces.IWebRequestContainerDetails {
        // this container is specifically created just to contain the per web request registrations
        // but it should also inherit all registrations from original container (ex; QueryBus, MutationBus, etc)
        const container = this._container.createChild();
        container.applyMiddleware(containerMiddleware);
        
        // IMPORTANT: Here we register the container with the request
        container.bind<Container>('Container').toConstantValue(container);
        container.bind<Request>('Request').toConstantValue(req);

        // these are registration from user of the framework directly (example: models)
        // _bindPerRequestRegistrations(container, this.__perRequestTypesRegistrations);

        return {
            bridgeModule: this,
            instance: container
        };
    }

    cleanup(container: inversifyInterfaces.Container): void {
        // container.unbind('Container');
        // container.unbind('Request');
        // this._containerModules.forEach(m => _unbindPerRequestRegistrations(container, m.__perRequestTypesRegistrations));
        container.unbindAll();
    }

}

// function _bindPerRequestRegistrations(container: Container, registrations: interfaces.GenericObject) {
//     Object.keys(registrations).forEach(key => {
//         container.bind(key).to(registrations[key]).inRequestScope();
//     });
// }

// function _unbindPerRequestRegistrations(container: inversifyInterfaces.Container, registrations: interfaces.GenericObject) {
//     try {
//         Object.keys(registrations).forEach(key => {
//             container.unbind(key);
//         });
//     } catch (e) {
//         console.error('Error unregistering service');
//     }
// }

function containerMiddleware(planAndResolve: inversifyInterfaces.Next): inversifyInterfaces.Next {
    return (args: inversifyInterfaces.NextArgs) => {
        console.log(`Resolving: ${args.serviceIdentifier.toString()}`);
        return planAndResolve(args);
    };
}

