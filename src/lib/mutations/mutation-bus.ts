import { Bridge } from '../bridge';
import { IBridgeRequest } from '../bridge.request';
import { Request } from 'express';
import { inject, injectable } from 'inversify';

import { Enforcer, IEnforcer } from '../enforcer';
import { interfaces } from '../interfaces';
import { IMutation } from './mutation';
import { IValidationResult } from './validation-result';


export interface IMutationBus {
    run < T > (req: IBridgeRequest, mutation: IMutation < T > , data: any, activity?: new () => interfaces.IActivity): Promise < any > ;
}

@injectable()
export class MutationBus implements IMutationBus {

    authorizedValue: any;
    errorBool: any;
    errorStr: any;
    logParams: any;

    public get enforcer(): IEnforcer {
        return this._enforcer;
    }

    constructor(@inject(Enforcer.name) private _enforcer: IEnforcer) {}

    run < T > (request: IBridgeRequest, mutation: IMutation < T > , data: any, activity?: new () => interfaces.IActivity): Promise < any > {
        const that = this;

        if (!activity) {
            return mutation.run(data).then((res: any) => res)
                .catch((e: any) => {
                    Bridge.logger.error(mutation.constructor.name, e);
                    return{ erros: [e.message] };
                });
        }

        // get activity instance
        const act: interfaces.IActivity = request.Container.instance.get(activity.name);

        // check activity authorization
        return this.enforcer.authorizationTo(act)
            .then((authorized) => {
                that.authorizedValue = authorized;
                if (!authorized) {
                    return Promise.reject(authorized);
                }

                // run the mutation validation
                return mutation.validate ? mutation.validate(data) : {
                    success: true
                };
            })
            .then((result: IValidationResult) => {
                // if it is valid
                if (!result.success) {
                    return Promise.reject(result);
                }

                return Promise.resolve(true);
            })
            .then((validated: boolean) => {
                return new Promise < any > ((resolve, reject) => {
                    (mutation as any).run(data).then((res: any) => {
                            resolve(res);
                        })
                        .catch((e: any) => {
                            Bridge.logger.error(mutation.constructor.name, e);
                            resolve({
                                erros: [e.message]
                            });
                        });
                });
            })
            .then((res: T) => {
                // TODO: Pending
                // that._logMutation(request, mutation);
                return res;
            })
            .catch((err) => {
                that.errorStr = err;
                // TODO: Pending
                // that._logMutation(request, mutation);
                return Promise.reject(err);
            });
    }

    // private _logMutation(request: IBridgeRequest, mutation) {
    //     if (mutation.log === true) {
    //         const user = request.user;
    //         const accessBy = user ? user.profile.firstName + ' ' + user.profile.lastName : '';

    //         const logParams = {
    //             timestamp: Date.now(),
    //             accessBy: accessBy,
    //             ipAddress: request.connection.remoteAddress,
    //             event: mutation.constructor.name,
    //             clientDetails: request.get('User-Agent'),
    //             eventType: 'mutation',
    //             payload: JSON.stringify(request.body),
    //             results: {
    //                 authorized: this.authorizedValue,
    //                 status: true,
    //                 details: this.errorStr || ('Success executing ' + mutation.constructor.name)
    //             }
    //         };

    //         const accessLogs = request.Container.instance.get < AccessLogs > (AccessLogs.name);

    //         accessLogs.model.create(logParams);
    //     }
    // }
}