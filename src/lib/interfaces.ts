import { interfaces as i } from '@atlas/common';
import { Request } from 'express';
import { Container, interfaces } from 'inversify';

import { IBridgeRequest } from './bridge.request';
import { enums } from './enums';
import { IMutation } from './mutations/mutation';
import { IMutationBus } from './mutations/mutation-bus';
import { IQuery } from './queries/query';
import { IQueryBus } from './queries/query-bus';

export namespace interfaces {

    /**
     *  GRAPHQL 
     */

    export interface IKeyType {
        [name: string]: i.Newable<any>;
    }

    export interface IBrigeData {
        types: IKeyType;
        inputs: IKeyType;
        queries: IKeyType;
        mutations: IKeyType;
        modules: i.Newable<any>[];
    }

    // GQL FIELD
    export interface IFieldDecorator {
        type: enums.GqlTypeEnum | i.Newable<any>;
        name?: string;
        isArray?: boolean;
        required?: boolean;
    }

    // GQL INPUT
    export interface IInputDecorator {
        name?: string;
    }

    export interface IInputMetadata {
        name: string;
        text: string;
        resolver: any;
    }

    // GQL TYPE
    export interface ITypeDecorator {
        name?: string;
    }

    // GQL RESOLVER
    export interface IResolverDecorator {
        forField: string;
    }

    export interface IActionOutput {
        type: i.Newable<any>;
        isArray?: boolean;
    }

    // GQL Query/Mutation Base
    export interface IActionDecorator {
        name?: string;
        activity?: i.Newable<IActivity>;
        parameters?: IParameterDefinition[];
        output: IActionOutput;
        text?: string;
    }

    export interface IActionMetadata {
        name: string;
        activity?: i.Newable<IActivity>;
        parameters?: IParameterDefinition[];
        text: string;
        types?: any[];
        resolver?: (root: any, args: any, ctx: IGraphqlContext) => Promise<any>
    }
    
    // GQL MUTATION
    export interface IMutationDecorator extends IActionDecorator { }
    
    // GQL QUERY
    export interface IQueryDecorator extends IActionDecorator { }

    // Bridge Module
    export interface IModuleDecorator extends i.Indexable<i.Newable<IAppModule>[] | i.Newable<IQuery<any>>[] | i.Newable<IMutation<any>>[] | i.Newable<IHttpController>[] | undefined > {
        imports ?: i.Newable<IAppModule>[]; // Array < new() => IAppModule > ; // IModuleOptions[];
        queries ?: i.Newable<IQuery<any>>[]; // Array < new(...args) => IQuery < any >> ;
        mutations ?: i.Newable<IMutation<any>>[]; // Array < new(...args) => IMutation < any >> ;
        httpControllers ?: i.Newable<IHttpController>[]; // Array < new(...args) => IHttpController >;
    }

    // PARAMETER
    export interface IParameterDefinition {
        name: string;
        type: any;
        required?: boolean;
        isArray?: boolean;
    }

    export interface IAppModule { }

    export interface IGraphqlContext {
        config: any;
        req: IBridgeRequest;
        requestContainer: IWebRequestContainerDetails;
        mutationBus: IMutationBus;
        queryBus: IQueryBus;
    }

    /**
     *  HTTP 
     */

    // HTTP ROUTE
    export interface IRouteDecorator {
        path: string;
        verb: enums.HttpVerbEnum;
        method: string;
        activity?: new () => IActivity;
    }

    export interface IParamMetadata {
        name: string;
        from: enums.FromTypeEnum;
        parameterIndex?: number;
    }

    export interface IRouteMetadata {
        path: string;
        verb: enums.HttpVerbEnum;
        method: string;
        params?: IParamMetadata[];
        activity?: new () => IActivity;
    }
 
    // HTTP CONTROLLER
    export interface IHttpControllerDecorator {
        prefix: string;
        activity?: new () => IActivity;
        routes: IRouteDecorator[];
    }

    export interface IHttpController { }
    
    export interface IHttpControllers {
        [name: string]: IHttpControllerDecorator;
    }
    
    
    // SECURITY
    export interface IActivity {
        check(): Promise<boolean>;
    }

    /**
     * CORE
     */
    
    export type GenericObject = { [key: string]: any };

    export interface IBridgeContainer {
        register<T extends i.Newable<any>>(type: T): void;
        registerSingleton<T extends i.Newable<any>>(type: T): void;
        registerPerWebRequest<T extends i.Newable<any>>(type: T): void;
        registerConstant<T extends GenericObject>(identifier: string, value: T): void;
        deregister<T extends i.Newable<any>>(identifier: string | T): void;
    
        get<T>(identifier: string): T;
        getBridgeContainerForWebRequest(req: Request): IWebRequestContainerDetails;
        cleanup(container: interfaces.Container): void;
    }

    export interface IWebRequestContainerDetails {
        instance: interfaces.Container;
        bridgeModule: IBridgeContainer;
    }

    
    export interface IDIRegistrator {
        bind: interfaces.Bind;
        unbind: interfaces.Unbind;
        isBound: interfaces.IsBound;
        rebind: interfaces.Rebind;
    }

}