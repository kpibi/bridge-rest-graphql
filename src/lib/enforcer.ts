import { Bridge } from './bridge';

import { injectable } from 'inversify';

import { interfaces } from './interfaces';

export interface IAuthorizationResult {
    err?: any;
    authorized?: boolean;
}

export interface IEnforcer {
    authorizationTo(activity: interfaces.IActivity): Promise<boolean>;
}

@injectable()
export class Enforcer implements IEnforcer {

    constructor() { }

    authorizationTo(activity: interfaces.IActivity): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            // TODO: Implement the allow and deny global

            Bridge.logger.debug('Checking activity authorization');

            activity.check().then((authorized) => {
                resolve(authorized);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
