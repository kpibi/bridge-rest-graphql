import 'reflect-metadata';

import { enums as e, interfaces as i } from '@atlas/common';
import { ApolloServer } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
// import * as cors from 'cors';
import * as express from 'express';
import { Container } from 'inversify';

import { BridgeContainer } from './bridge-container';
import {
    _getControllerMetadata,
    _getModuleMetadata,
    _getMutationMetadata,
    _getMutations,
    _getQueries,
    _getQueryMetadata,
} from './bridge-metadata';
import { IBridgeRequest } from './bridge.request';
import { Enforcer } from './enforcer';
// import { enums } from './enums';
import { getTypesAndResolvers } from './graphql/graphql-schema-generator';
import { interfaces } from './interfaces';
import { IMutation } from './mutations/mutation';
import { MutationBus } from './mutations/mutation-bus';
import { IQuery } from './queries/query';
import { QueryBus } from './queries/query-bus';

const winston = require('winston');
// const getParameterNames = require('get-parameter-names');


export interface IFrameworkOptions {
    port ? : number;
    logLevel ? : 'debug' | 'info' | 'warning' | 'error';
    bodyParserLimit ? : string;
    graphqlPath ? : string;
    graphqlContextExtra ? : any;
}

const defaultServerOptions: IFrameworkOptions = {
    port: 8081,
    logLevel: 'error',
    bodyParserLimit: '50mb',
    graphqlPath: '/graphql'
};

interface BridgeArtifacts extends i.Indexable< any[] > {
    mutations: i.Newable<IMutation<any>>[];
    queries: i.Newable<IQuery<any>>[];
    httpControllers: any[];
}

export class Bridge {

    public static logger: any;
    
    private static _loggerLevel = e.LoggerLevelEnum.info;
    private static Server = express();
    private static _bridgeContainer: BridgeContainer;

    public static setLoggerLevel(level: e.LoggerLevelEnum) {
        if (!level) return;
        this._loggerLevel = level;
    }

    static get bridgeContainer(): BridgeContainer {
        return Bridge._bridgeContainer;
    }

    static create(appModule: new() => interfaces.IAppModule, options ? : IFrameworkOptions): Bridge | null {
        
        this.logger = winston.createLogger({
            level: this._loggerLevel,
            transports: [
                new winston.transports.Console({
                    format: winston.format.simple()
                }),
                new winston.transports.File({
                    filename: 'combined.log'
                })
            ]
        });

        // merge frameowrk options
        const newOptions = Object.assign({}, defaultServerOptions, options);
        // create inversify container
        const container = new Container({
            autoBindInjectable: true
        });
        // get main module metadata
        const moduleDefinition = _getModuleMetadata(appModule);// Reflect.getMetadata(METADATA_KEY.module, appModule) as interfaces.IModuleDecorator;

        // obtain module artifacts recursively
        const artifacts: BridgeArtifacts | undefined = getModuleArtifacts(moduleDefinition);

        if (!artifacts) {
            this.logger.error('Bridge did not find any query, mutation or http controller it can use to run');
            return null;
        }

        // create dependency injection container and process the registrations
        this._bridgeContainer = new BridgeContainer(container);
        registerDiDependencies(this._bridgeContainer);

        return new Bridge(this._bridgeContainer, artifacts, Bridge.Server, newOptions);
    }

    private _options: IFrameworkOptions;

    private constructor(
        private _container: BridgeContainer,
        private _artifacts: BridgeArtifacts,
        private _server: express.Express,
        options ? : IFrameworkOptions) {

        this._options = Object.assign({}, defaultServerOptions, options);

        // middlewares
        this._server.use(cleanup);
        this._server.use(setBridgeContainer);
        // this._server.use('*', cors());
        this._server.use(bodyParser.urlencoded({
            extended: false,
            limit: this._options.bodyParserLimit
        }));
        this._server.use(bodyParser.json({
            limit: this._options.bodyParserLimit
        }));

    }

    get Container(): BridgeContainer {
        return Bridge._bridgeContainer; // this._container;
    }

    start() {

        // generate graphql schema
        const graphqlSchema = 
            getTypesAndResolvers(this._artifacts.mutations, this._artifacts.queries);
        
        if (graphqlSchema) {
            const apolloServer = new ApolloServer({
                ...graphqlSchema,
                context: ({ req }: { req: express.Request }) => ({
                    req: req,
                    requestContainer: (req as IBridgeRequest).Container,
                    mutationBus: this._container.get(MutationBus.name),
                    queryBus: this._container.get(QueryBus.name)
                }),
                debug: true,
                playground: true,
            });

            apolloServer.applyMiddleware({ 
                app: this._server,
                cors: true,
            });

            const _httpServer = this._server.listen(this._options.port, () => console.log(
                `Bridge Server is now running on http://localhost:${this._options.port}/${this._options.graphqlPath}`
            ));
        }

        // process http controllers
        // if (this._artifacts.httpControllers && this._artifacts.httpControllers.length) {
        //     this._artifacts.httpControllers.forEach(c => injectHttpController(c, this._server, this._container));
        //     Bridge.logger.debug(`HTTP Controllers: ${this._artifacts.httpControllers.map(c => (c as any).name).sort().join(', ')}`);
        // }

        
    }

    get server(): express.Express {
        return this._server;
    }
}

function getModuleArtifacts(mod: interfaces.IModuleDecorator): BridgeArtifacts | undefined {
    // process http controllers
    if (!mod) return;

    const result: BridgeArtifacts = {
        mutations: [],
        queries: [],
        httpControllers: []
    };

    const modules = [ mod ];

    if (mod.imports) {
        mod.imports.forEach(m => {
            const moduleMetadata = _getModuleMetadata(m); // Reflect.getMetadata(METADATA_KEY.module, appModule) as interfaces.IModuleDecorator;
            modules.push(moduleMetadata);
        });
    }

    const artifactTypes = ['mutations', 'queries', 'httpControllers'];
    modules.forEach(module => {
        artifactTypes.forEach(type => {
            if (module[type]) {
                result[type] = result[type].concat(module[type]);
            }
        });
    });

    return result;
}

function registerDiDependencies(container: BridgeContainer) {
    container.registerSingleton(Enforcer);
    container.registerSingleton(MutationBus);
    container.registerSingleton(QueryBus);

    // auto register queries and mutations with the dependency injector container
    _getQueries().forEach(q => {
        const metadata = _getQueryMetadata(q); // Reflect.getMetadata(metadataKey, a);

        if (metadata.activity)
            container.registerPerWebRequest(metadata.activity);

        container.registerPerWebRequest(q);
    });

    _getMutations().forEach(q => {
        const metadata = _getMutationMetadata(q); // Reflect.getMetadata(metadataKey, a);

        if (metadata.activity)
            container.registerPerWebRequest(metadata.activity);

        container.registerPerWebRequest(q);
    });
}

function setBridgeContainer(req: express.Request, res: express.Response, next: any) {
    (req as IBridgeRequest).Container = Bridge.bridgeContainer.getBridgeContainerForWebRequest(req);
    next();
}

function cleanup(req: express.Request, res: express.Response, next: () => void) {
    res.on('finish', function () {
        // perform your cleanups...
        const containerDetails = (req as IBridgeRequest).Container;
        containerDetails.bridgeModule.cleanup(containerDetails.instance);
        delete((req as IBridgeRequest).Container);
    });

    next();
}

// function injectHttpController(c: i.Newable < interfaces.IHttpController > , server: express.Express, container: BridgeContainer) {
//     const controllerName = c.name;
//     const controllerMetadata = _getControllerMetadata(c); // Reflect.getMetadata(METADATA_KEY.controller, c); // BRIDGE.http.controllers[controllerName];

//     if (!controllerMetadata) {
//         throw new Error(`Controller ${controllerName} it has not been decorater correctly with the @Controller decorator`);
//     }

//     if (controllerMetadata.prefix === '/grahql') {
//         throw new Error(`Controller prefix '/graphql' it is not allowed for http controllers`);
//     }

//     // register controller with the container
//     container.register(c as any);
//     // register controller's activity if neccessary
//     if (controllerMetadata.activity) {
//         container.register(controllerMetadata.activity);
//     }

//     // instantiate controllers and register routes
//     const router = express.Router();

//     controllerMetadata.routes.forEach(r => {
//         // register mrthod activity
//         if (r.activity) {
//             container.register(r.activity);
//         }

//         router[r.verb](r.path, (req: express.Request, res: express.Response, next: express.NextFunction) => {
//             try {
//                 // get container
//                 const container = (req as IBridgeRequest).Container.instance;
//                 // instantiate controller
//                 const controller = container.get < interfaces.IHttpController > (controllerName);
//                 // get route method
//                 const method = (controller as any)[r.method];
//                 // process activity if neccessary
//                 let activityPromise = {
//                     check: () => Promise.resolve(true)
//                 } as interfaces.IActivity;

//                 if (r.activity) {
//                     // method activity has priority over controller activity
//                     activityPromise = container.get(r.activity.name);
//                 } else if (controllerMetadata.activity) {
//                     // if there is not method activity but and there is a controller activity then process it
//                     activityPromise = container.get(controllerMetadata.activity.name);
//                 }

//                 activityPromise.check().then(allowed => {
//                         if (!allowed) {
//                             return res.status(401).send('Unauthorized');
//                         }

//                         // get parameters to be passed to the controller method
//                         const parameters = getParameterValues(req, r, controllerMetadata, method);
//                         // execute method with the rigth context
//                         let result = method.apply(controller, parameters);
//                         // process result
//                         Promise.resolve(result).then(r => {
//                             if (typeof r === 'string') {
//                                 res.send(r);
//                             } else {
//                                 res.json(JSON.stringify(r));
//                             }
//                         }).catch(e => {
//                             res.status(500).send('Internal error');
//                             console.error(e);
//                         });
//                     })
//                     .catch(e => {
//                         console.error('There was an error processing activity', e);
//                         res.status(500).send('Internal error');
//                     });

//             } catch (e) {
//                 console.error(`Unexpected error while executing http method`, e);
//                 res.status(500).send('Internal error');
//             }
//         });
//     });

//     server.use(controllerMetadata.prefix, router);
// }

// function getParameterValues(
//     req: express.Request,
//     route: interfaces.IRouteMetadata,
//     controllerMetadata: interfaces.IHttpControllerDecorator,
//     method: Function): any[] {
//     // get method names
//     const functionParameters: string[] = getParameterNames(method);

//     if (!route.params || !functionParameters.length) return [];

//     const parameters: { [name: string]: any } = {};
//     let value: any;

//     route.params.map(p => {
//         switch (p.from) {
//             case enums.FromTypeEnum.param:
//                 const originalUrlTokens = req.originalUrl.replace(controllerMetadata.prefix, '').split('/');
//                 const pathTokens = route.path.split('/')
//                 const paramPosition = pathTokens.findIndex(v => v.replace(':', '') === p.name);
//                 value = originalUrlTokens[paramPosition];
//                 break;
//             case enums.FromTypeEnum.query:
//                 value = req.query[p.name];
//                 break;
//             case enums.FromTypeEnum.body:
//                 value = req.body[p.name];
//                 break;
//             case enums.FromTypeEnum.header:
//                 value = req.headers[p.name];
//                 break;
//         }

//         parameters[p.name] = value;
//     });

//     return functionParameters.map(paramName => parameters[paramName]);
// }