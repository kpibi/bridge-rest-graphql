import { Bridge } from '../bridge';
import { METADATA_KEY } from '../constants';
import { enums } from '../enums';
import { interfaces } from '../interfaces';
import { _addHttpController } from '../bridge-metadata';

import { interfaces as i } from '@atlas/common';


const getParameterNames = require('get-parameter-names');

export function httpController(prefix: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: i.Newable<any>) {
        let routes: interfaces.IRouteMetadata[] = Reflect.getMetadata(METADATA_KEY.httpMethod, target.prototype);

        const metadata: interfaces.IHttpControllerDecorator = {
            prefix: prefix,
            activity: activity,
            routes: routes
        };

        _addHttpController(target, metadata);
    };
}

export function get(path: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: any, method: string) {
        updateRouteMetadata(target, path, method, enums.HttpVerbEnum.Get, activity);
    };
}

export function post(path: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: any, method: string) {
        updateRouteMetadata(target, path, method, enums.HttpVerbEnum.Post, activity);
    };
}

export function put(path: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: any, method: string) {
        updateRouteMetadata(target, path, method, enums.HttpVerbEnum.Put, activity);
    };
}

export function patch(path: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: any, method: string) {
        updateRouteMetadata(target, path, method, enums.HttpVerbEnum.Patch, activity);
    };
}

export function httpDelete(path: string, activity?: i.Newable<interfaces.IActivity>) {
    return function(target: any, method: string) {
        updateRouteMetadata(target, path, method, enums.HttpVerbEnum.Delete, activity);
    };
}

// parameters

export function fromQuery(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    updateParamMetadata(target, enums.FromTypeEnum.query, propertyKey, parameterIndex);
}

export function fromParam(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    updateParamMetadata(target, enums.FromTypeEnum.param, propertyKey, parameterIndex);
}

export function fromBody(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    updateParamMetadata(target, enums.FromTypeEnum.body, propertyKey, parameterIndex);
}

export function fromHeader(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    updateParamMetadata(target, enums.FromTypeEnum.header, propertyKey, parameterIndex);
}



function updateRouteMetadata(target: any, path: string, method: string, verb: enums.HttpVerbEnum, activity?: i.Newable<interfaces.IActivity>) {
    let routes = Reflect.getMetadata(METADATA_KEY.httpMethod, target) || [];
    let params = Reflect.getMetadata(METADATA_KEY.params, target, method) || [];
    const fromParams = getFromParamValues(target, path) || [];
    
    if (params.length || fromParams.length) {
        params = params.concat(fromParams);
    }

    if (!params.length) params = null;

    routes.push({ path, verb, method, activity, params });
    Reflect.defineMetadata(METADATA_KEY.httpMethod, routes, target);
}

function updateParamMetadata(target: any, from: enums.FromTypeEnum, propertyKey: string | symbol, parameterIndex: number) {
    const params: interfaces.IParamMetadata[] = Reflect.getMetadata(METADATA_KEY.params, target, propertyKey) || [];
    const parameterNames = getParameterNames((target as any)[propertyKey]);
    
    params.push({
        name: parameterNames[parameterIndex],
        from, 
        parameterIndex
    });

    Reflect.defineMetadata(METADATA_KEY.params, params, target, propertyKey);
}

function getFromParamValues(target: any, path: string) {
    if (!path) return;
    
    const params: interfaces.IParamMetadata[] = [];

    // extract parameters
    const paramRegex = new RegExp(/:[a-zA-Z]\w+/g);
    let result;

    while(result = paramRegex.exec(path)) {

        params.push({
            name: result[0].replace(':', ''),
            from: enums.FromTypeEnum.param
        });
    }

    if (!params.length) return;

    return params;
}
