import * as Hbs from 'handlebars';

import { Bridge } from '../bridge';
import { METADATA_KEY } from '../constants';
import { interfaces } from '../interfaces';
import { enums } from '../enums';
import { _addInput, _addType, _addMutation, _addQuery } from '../bridge-metadata';

import { interfaces as i } from '@atlas/common';


export function GqlField(definition?: interfaces.IFieldDecorator) {
    return function(target: any, property: string) {
        if (!definition) {
            definition = { type: enums.GqlTypeEnum.String };
        }

        if (!definition.type) {
            definition.type = enums.GqlTypeEnum.String;
        }

        let typeName = (definition.type as i.Newable<any>).name || definition.type;

        if (definition.isArray) {
            typeName = `[${typeName}]`;
        }

        if (definition.required) {
            typeName += '!';
        }

        let fields = Reflect.getMetadata(METADATA_KEY.field, target);

        if (!fields) {
            fields = {};
        }

        const name = definition.name || property;
        fields[name] = typeName;

        Reflect.defineMetadata(METADATA_KEY.field, fields, target);
    };
}

export function GqlResolver(definition?: interfaces.IResolverDecorator) {
    return function(target: i.Newable<any>, method: string) {
        if (!definition) {
            console.warn('Resolver defined without specifying a field');
            return;
        }

        let resolvers = Reflect.getMetadata(METADATA_KEY.resolver, target) || {};

        resolvers[definition.forField] = (target as any)[method];
        Reflect.defineMetadata(METADATA_KEY.resolver, resolvers, target);
    };
}

export function GqlInput(definition?: interfaces.IInputDecorator) {
    return function(target: i.Newable<any>) {
        processInputAndType(METADATA_KEY.input, target, definition);
    };
}

export function GqlType(definition?: interfaces.ITypeDecorator) {
    return function(target: i.Newable<any>) {
        processInputAndType(METADATA_KEY.type, target, definition);        
    };
}

export function GqlMutation(definition: interfaces.IMutationDecorator) {
    return (target: i.Newable<any>) => {
        processQueryAndMutation(target, METADATA_KEY.mutation, definition);
    };
}

export function GqlQuery(definition: interfaces.IQueryDecorator) {
    return (target: i.Newable<any>) => {
        processQueryAndMutation(target, METADATA_KEY.query, definition);
    };
}

function processInputAndType(metadataKey: string, target: i.Newable<any>, definition?: interfaces.IInputDecorator) {
    const templateType = metadataKey === METADATA_KEY.input ?  'input' : 'type';
    const templateText = `${templateType} {{typeName}} {
        {{#each fields}}
        {{this}}
        {{/each}}
    }`;

    if (!definition) {
        definition = {};
    }

    const name = definition.name || target.name;
    const fields = Reflect.getMetadata(METADATA_KEY.field, target.prototype);
    const fieldNames = Object.keys(fields);
    const payload = {
        typeName: name,
        fields: fieldNames.map(f => `${f}: ${fields[f]}`)
    };
    const graphQlText = Hbs.compile(templateText)(payload);
    

    switch (metadataKey) {
        case METADATA_KEY.input:
            _addInput(target, graphQlText);
            break;
        case METADATA_KEY.type:
            _addType(target, graphQlText);            
            break;
    }
}

function getComplexFieldNames(fields: any) {
    const nonComplexFields = ['String', 'String!', 'Int', 'Int!'];
    const result = [];

    for (let key in fields) {
        if (nonComplexFields.indexOf(fields[key]) === -1 ) {
            result.push(fields[key]);
        }
    }

    return result;
}

function processQueryAndMutation(target: i.Newable<any>, type: string, definition: interfaces.IActionDecorator) {
    const name = definition.name || target.name;
    let parameters;

    // add only complex types
    const types: i.Newable<any>[] = [];

    // add types for parameters
    if (definition.parameters) {
        parameters = definition.parameters.map(p => {
            const isArray = p.isArray;
            return p.isArray ?
                `${p.name}: [${p.type.name || p.type}]${p.required ? '!' : ''}`
                : `${p.name}: ${p.type.name || p.type}${p.required ? '!' : ''}`;
        });

        definition.parameters.forEach(p => {
            if (p.type.name) {
                types.push(p.type);
            }
        });
    }


    // const parameters = definition.parameters.map(p => `${p.name}: ${p.type.name}`);
    const inputTemplateText = parameters !== undefined ?
        `{{name}}({{#each parameters}}{{this}},{{/each}}): {{output}}`
        : `{{name}}: {{output}}`;

    const output = definition.output.isArray ? 
        `[${definition.output.type.name}]` :
        definition.output.type.name !== undefined ? definition.output.type.name : definition.output.type;
    const payload = { name, parameters, output };
    let graphqlType = Hbs.compile(inputTemplateText)(payload);

    // remove extra comma
    graphqlType = graphqlType.replace(',)', ')');

    // add output type
    // if (definition.output.name) {
    //     types.push(definition.output);
    // }

    updateQueriesAndMutationsMetadata(type, target, name, graphqlType, definition.activity, types);
}

function updateQueriesAndMutationsMetadata(
    metadataType: string, 
    target: any,
    graphqlName: string, graphqlText: string,
    activity?: i.Newable<interfaces.IActivity>,
    types?: any[]) {

        const metadata: interfaces.IActionMetadata = {
            name: graphqlName,
            text: graphqlText,
            activity: activity,
            types: types
        };
        
    switch (metadataType) {
        case METADATA_KEY.mutation:
            _addMutation(target, metadata);
            break;
        case METADATA_KEY.query:
            _addQuery(target, metadata);
            break;
    }
}
