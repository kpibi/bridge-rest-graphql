import { interfaces as i } from '@atlas/common';

import { _addModule } from '../bridge-metadata';
import { interfaces } from '../interfaces';

export class ModuleBase implements interfaces.IAppModule {
    
}

export function BridgeModule(options: interfaces.IModuleDecorator) {
    return function(target: i.Newable<interfaces.IAppModule>) {
        _addModule(target, options);
    };
}
