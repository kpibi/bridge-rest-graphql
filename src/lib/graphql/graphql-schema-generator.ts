import { gql } from 'apollo-server-express';
import { DocumentNode } from 'graphql';
import { IResolvers } from 'graphql-tools';

import { Bridge } from '../bridge';
import {
    _getInputMetadata,
    _getInputs,
    _getMutationMetadata,
    _getMutations,
    _getQueries,
    _getQueryMetadata,
    _getTypeMetadata,
    _getTypes,
    _saveSchema,
} from '../bridge-metadata';
import { interfaces } from '../interfaces';
import { IMutation } from '../mutations/mutation';
import { IQuery } from '../queries/query';

import { interfaces as i } from '@atlas/common';

export interface ITypesAndResolvers {
    typeDefs?: DocumentNode,
    resolvers?: IResolvers,
}

export function getTypesAndResolvers(
    mutationClasses: i.Newable<IMutation<any>>[], 
    queryClasses: i.Newable<IQuery<any>>[]): ITypesAndResolvers | null {

    const inputs = _getInputs().map(input => _getInputMetadata(input));
    const types = _getTypes().map(type => _getTypeMetadata(type));
    const mutations = mutationClasses.map(mutation => _getMutationMetadata(mutation));
    const queries = queryClasses.map(query => _getQueryMetadata(query));

    if ((!mutations || !mutations.length) && (!queries || !queries.length)) {
        return null;
    }

    // shoe some debug info
    if (queries && queries.length)
        Bridge.logger.debug(`Queries: ${queries.map(q => q.name).sort().join(', ')}`);

    if (mutations && mutations.length)
        Bridge.logger.debug(`Mutations: ${mutations.map(q => q.name).sort().join(', ')}`);

    let schema = `
    ${inputs.join('\n')}

    ${types.map(t => t.text).join('\n')}`;

    if (mutations && mutations.length) {
        schema += `
            type Mutation {
                ${mutations.map(m => m.text).join('\n')}
            }`;
    }

    if (queries && queries.length) {
        schema += `
            type Query {
                ${queries.map(q => q.text).join('\n')}
            }`;
    }

    let basicSchema = `
        schema {
            query: Query
            mutation: Mutation
        }
    `;

    if (!queries || !queries.length) {
        basicSchema = basicSchema.replace('query: Query', '');
    }

    if (!mutations || !mutations.length) {
        basicSchema = basicSchema.replace('mutation: Mutation', '');
    }

    schema += basicSchema;

    // for debugging purposes
    Bridge.logger.debug(`Schema: \n${schema}`);

    const typeDefs = gql(schema);
    const resolvers = mergeModuleResolvers(types, { mutations, queries });

    return {
        resolvers,
        typeDefs,
    }
}

interface IMutationAndQueries extends i.Indexable<interfaces.IActionMetadata[]> { 
    mutations: interfaces.IActionMetadata[];
    queries: interfaces.IActionMetadata[];
}

// --- MERGE RESOLVERS
function mergeModuleResolvers(types: interfaces.IInputMetadata[], mutationAndQueries: IMutationAndQueries ) {

    const resolvers: { [name: string]: any } = {};

    // complex type resolvers
    types.forEach(t => {
        if (t.resolver) {

            if (!resolvers[t.name]) {
                resolvers[t.name] = {};
            }

            const objectResolvers = resolvers[t.name];

            for (let resolverKey in t.resolver) {
                objectResolvers[resolverKey] = t.resolver[resolverKey];
            }
        }
    });

    // mutation and queries resolvers
    ['mutations', 'queries'].forEach(metadataType => {
        if (!mutationAndQueries[metadataType] || mutationAndQueries[metadataType].length === 0) {
            return;
        }

        const resolverType = metadataType === 'mutations' ? 'Mutation' : 'Query';
        const target: i.Indexable<any> = resolvers[resolverType] = {};

        mutationAndQueries[metadataType].forEach((queryOrMutation: interfaces.IActionMetadata) => {
            target[queryOrMutation.name!] = queryOrMutation.resolver;
        });
    });

    return resolvers;
}