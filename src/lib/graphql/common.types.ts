import { GqlType, GqlField } from '../decorators';
import { enums } from '../enums';

@GqlType()
export class ErrorDetails {
    @GqlField({ type: enums.GqlTypeEnum.String })
    field!: string;

    @GqlField({ type: enums.GqlTypeEnum.String, isArray: true, required: true })
    errors!: string[];
}