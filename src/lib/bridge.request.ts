import { Request } from 'express';

import { interfaces } from './interfaces';

export interface IBridgeRequest extends Request {
    /**
     * Bridge Container
     */
    Container: interfaces.IWebRequestContainerDetails;

}