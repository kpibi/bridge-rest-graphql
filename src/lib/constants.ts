export const METADATA_KEY = {
    bridge: 'bridge:framework',
    module: 'bridge:module',
    query: 'bridge:query',
    mutation: 'bridge:mutation',
    type: 'bridge:type',
    field: 'bridge:field',
    resolver: 'bridge:resolver',
    input: 'bridge:input',
    controller: 'bridge:http-controller',
    httpMethod: 'bridge:http-method',
    params: 'bridge:param'
}