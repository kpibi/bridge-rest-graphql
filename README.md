# About

Bridge is a small framework developed for NodeJS that will help you to create your backend APIs using either GraphQL or Rest in a very intuitive way giving you modern tools such as logging, dependency injection, etc. This is not the type of framework made for quick and dirty start even though it is pretty easy to understand and use. The main idea behind it is to remove a lot of boilerplate out of your way helping you to focus on the business logic of your app and at the same time offers you the main building blocks that we have to deal with when building a backend application using NodeJS. 

The framework is heavily based on `decorators` which are used to annotate the different type of objects you can create with Bridge. I know there is a love/hate realtionship out there when it comes to this topic but in this case their use was pretty handy.

# Motivation

One year ago I found out about GraphQL and I inmediately felt in love with it. Created a couple of app and everything was good until I found myself creating my typescript classes and then repeating my code when creating my inputs and types in graphql. Then I started working on this idea trying to reduce the repetitive code and the disconnection between my graphql inputs/types and my typescript code. In addition, another key motivation was the need to integrate Dependency Injection and take it closer to the SOLID principle.

# Installation

```
npm install bridge-rest-graphql --save
```
or
```
yarn add bridge-rest-graphql
```

# The Basis (GraphQL)

Enough talk, let see what Bridge can do for you:

### Step 1: Define your first graphql type:


```
@GqlType()
class Product {

    // Default data type for every field is String
    // this could also be used: @GqlField({ type: GqlTypeEnum.String })
    @GqlField()
    name!: string;

    @GqlField({ type: enums.GqlTypeEnum.Float })
    price!: number;
}
```

This is a pretty simple class with a couple of annotations that serve us for two purposes. As a standard typesxcript class and at the same time we are adding some graphql decorators.

### Step 2: Let's create a simple query to return a list of products:


```
@GqlQuery({
    name: 'products',
    output: { type: Product, isArray: true }
})
class GetProductsQuery implements IQuery<Product[]> {
    
    run(data: any): Promise<Product[]> {
        return Promise.resolve(products);
    }
}
```

With type we defined before now we can use in this simple GraphQL query to return our products

### Step 3: Now lets add a simple input to use it in pur mutation

```
@GqlInput()
class ProductInput extends Product { }
```

This is another plus here, as you can see we were able to use inheritance and it was not neccesary to redefine the same properties for our input type

### Step 4: Now let's add a very simple mutation:

```
@GqlMutation({
    name: 'addProduct',
    parameters: [
        { name: 'input', type: ProductInput }
    ],
    output: { type: Product }
})
class AddProductMutation implements IMutation<Product> {
    
    run(data: { input: ProductInput }): Promise<Product> {
        products.push(data.input);
        return Promise.resolve(data.input);
    }
}
```

I have used this variable called `products` this is a basically a simple product array. As you can see this is very simple. We are just addind all products to this array

### Step 5: Now that we have basic definitions we can create our first module:


```
@BridgeModule({
    queries: [ GetProductsQuery ]
})
class AppModule {}
```

### Step 6: Create a new Bridge App.

```
const app = Bridge.create(AppModule);

if (app)
    app.start();
```

And that's it, we have a new app running using Bridge and offering a a single query called: `products`. Notice how we checked if app was not undefined because if bridge finds any issue while bootstraping then the `create` will not return an instance instead it will be null.

# The Basis (Http Controllers)

### Step 1: Create Http Controller

```

/**
 * Simple Rest Product's Controller
 */
@httpController('/products')
export class ProductsController {

    @get('/')
    private index(): Promise<Product[]> {
        return Promise.resolve(products);
    }

    @get('/:id')
    private getById(id: string): Promise<Product | undefined> {
        return Promise.resolve(products.find(p => p.id === +id));
    }

    @post('/')
    private add(@fromBody product: Product): Promise<Product> {
        products.push(product);
        return Promise.resolve(product);
    }

    @put('/')
    private put(@fromBody name: string, @fromBody price: number): Promise<Product | null> {
        var productIndex = products.findIndex(p => p.name === name);

        if (productIndex === -1) return Promise.resolve(null);

        products[productIndex] = { id: 5, name: name, price: price };

        return Promise.resolve(products[productIndex]);
    }

    @httpDelete('/:id')
    private delete(id: string): Promise<boolean> {
        var productIndex = products.findIndex(p => p.id === +id);

        if (productIndex === -1) return Promise.resolve(false);

        products.splice(productIndex, 1);

        return Promise.resolve(true);
    }
}
```

As you can see there are a few extra decorators there that are pretty helpful to bring the there from a request right to where you need it such as: `@fromBody`, `@fromParam`, `@fromQuery`, `@fromHeader`. You can learn more about them in the Http Controller Section (in progress)

### Step 2: Create our bridge module to include the controller

```
/**
 * Bridge Module definition 
 */
@BridgeModule({
    httpControllers: [ ProductsController ]
})
class AppModule {}
```

### Step 3: Now we can just create the app and start it

```
const app = Bridge.create(AppModule);

if (app)
    app.start();
```

# Features and API 

* Support for GraphQL (coming soon)
* Support for HTTP Controllers (coming soon)
* Support for Dependency Injection (coming soon)
* Bridge API (coming soon)

# Support

If you are experience any kind of issues I will be happy to help. You can report an issue using the [issues page](https://bitbucket.org/kpibi/bridge-rest-graphql/issues). 

# Acknowledgements

Thanks a lot to all developers out there who share their efforts with others and add their contribution to the open source community. This project would not exist without the help from others projects (check dependencies section)

# Main Dependencies

* [Typescript](https://github.com/Microsoft/TypeScript)
* [Express](https://github.com/expressjs/express/)
* [Apollo Server](https://github.com/apollographql/apollo-server)
* [Inversify](https://github.com/inversify/InversifyJS)
* [Winston](https://github.com/winstonjs/winston)
* [Reflect Metadata](https://github.com/rbuckton/reflect-metadata)

# License


License under the MIT License (MIT)

Copyright © 2012-2018 [Orlando Quero](https://www.linkedin.com/in/orlando-m-quero)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 

IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.